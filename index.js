const { MongoClient } = require("mongodb");

function MongoConnector(config) {
  this.config = config;
}

MongoConnector.prototype.Connect = async function() {
  let connectionString = "mongodb://";
  if (this.config.mongoAuth) {
    connectionString = `${connectionString}${this.config.mongoUsername}:${
      this.config.mongoPassword
    }@`;
  }
  for (const url of this.config.mongoURL) {
    connectionString = `${connectionString}${url},`;
  }
  connectionString = connectionString.slice(0, connectionString.length - 1);
  if (this.config.mongoDatabase) {
    connectionString = `${connectionString}/${this.config.mongoDatabase}`;
  }
  if (this.config.mongoAuth && this.config.mongoAuthSource) {
    connectionString = `${connectionString}?authSource=${
      this.config.mongoAuthSource
    }`;
    if (this.config.ssl) {
      connectionString = `${connectionString}&ssl=true`;
    }
    if (this.config.rs) {
      connectionString = `${connectionString}&replicaSet=${this.config.rs}`;
    }
  }
  this.connectionString = connectionString;

  try {
    this.connection = await MongoClient.connect(this.connectionString, {
      reconnectTries: 60,
      reconnectInterval: 1000
    });

    this.database = await this.connection.db(this.config.mongoDatabase);
  } catch (err) {
    error(err);
  }

  return this.connectionString;
};

MongoConnector.prototype.find = async function(collection, object) {
  return await this.database.collection(collection).find(object);
};

MongoConnector.prototype.page = async function(
  collection,
  object,
  page,
  pageSize
) {
  return await this.database
    .collection(collection)
    .find(object)
    .limit(pageSize)
    .skip(page * pageSize);
};

MongoConnector.prototype.update = async function(collection, old, newd) {
  return await this.database.collection(collection).update(old, newd);
};

MongoConnector.prototype.count = async function(collection, doc) {
  return await this.database.collection(collection).count(doc);
};

MongoConnector.prototype.delete = async function(collection, doc) {
  return await this.database.collection(collection).remove(doc);
};

MongoConnector.prototype.create = async function(collection, doc) {
  return await this.database.collection(collection).insert(doc);
};

MongoConnector.prototype.aggregate = async function(collection, pipeline) {
  return await this.database.collection(collection).aggregate(pipeline);
};

module.exports = MongoConnector;
