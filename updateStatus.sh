if [ $# -ne 3 ]
then
  echo "Usage:";
  echo "$0 <username> <password> <status>"
  exit
fi

curl -X POST -H 'Content-Type: application/json' -d '{ "state": "'$3'", "url": "http://ci.froice.uk/", "key": "Froice CI"}' --user $1:$2 "https://api.bitbucket.org/2.0/repositories/froice/mongoConnector/commit/$(git rev-parse --short HEAD)/statuses/build";
