jest.unmock("bson");
jest.unmock("mongo-mock");
jest.unmock("../index");
jest.mock("mongodb", () => require("mongo-mock"));

describe("mongoConnector", () => {
  let mc;
  const MC = require("../index");
  let defaultConfig = {
    mongoURL: ["mongo1", "mongo2", "mongo3"],
    mongoUsername: "user1",
    mongoPassword: "pass1",
    mongoDatabase: "db1",
    mongoAuth: true,
    mongoAuthSource: "admin",
    ssl: true,
    rs: "shard-0"
  };
  let noSSLConfig = {
    mongoURL: ["mongo1", "mongo2", "mongo3"],
    mongoUsername: "user1",
    mongoPassword: "pass1",
    mongoDatabase: "db1",
    mongoAuth: true,
    mongoAuthSource: "admin",
    ssl: false,
    rs: "shard-0"
  };
  beforeEach(() => {});

  it("should create a connection with SSL", () => {
    mc = new MC(defaultConfig);
    mc.Connect();
    expect(mc.connectionString).toEqual(
      "mongodb://user1:pass1@mongo1,mongo2,mongo3/db1?authSource=admin&ssl=true&replicaSet=shard-0"
    );
    expect(mc.config).toBeDefined();
  });

  it("should create a connection with SSL no Auth", () => {
    mc = new MC(Object.assign(defaultConfig, { mongoAuth: false }));
    mc.Connect();
    expect(mc.connectionString).toEqual("mongodb://mongo1,mongo2,mongo3/db1");
    expect(mc.config).toBeDefined();
  });

  it("should create a connection without SSL", () => {
    mc = new MC(noSSLConfig);
    mc.Connect();
    expect(mc.connectionString).toEqual(
      "mongodb://user1:pass1@mongo1,mongo2,mongo3/db1?authSource=admin&replicaSet=shard-0"
    );
    expect(mc.config).toBeDefined();
  });

  it("should create a connection without SSL no Auth", () => {
    mc = new MC(Object.assign(noSSLConfig, { mongoAuth: false }));
    mc.Connect();
    expect(mc.connectionString).toEqual("mongodb://mongo1,mongo2,mongo3/db1");
    expect(mc.config).toBeDefined();
  });
  describe("should perform database functions", () => {
    let insert;
    let find;
    let aggregate;
    let del;
    let update;

    beforeEach(() => {
      mc = new MC(Object.assign(defaultConfig), { mongoAuth: false });
      mc.Connect();
      mc.create("test", { test: true, abc: "def" });

      insert = jest.fn();
      find = jest.fn();
      aggregate = jest.fn();
      del = jest.fn();
      update = jest.fn();

      mc.database = {
        collection: jest.fn(() => ({
          insert: insert,
          find: find,
          aggregate: aggregate,
          delete: del,
          update: update
        }))
      };
    });
    it("should define mc", () => {
      expect(mc.connectionString).toEqual("mongodb://mongo1,mongo2,mongo3/db1");
    });
    it("should count documents", () => {
      const x = mc.create("test", { abc: "def" });
      expect(mc.database.collection).toBeCalled();
      expect(mc.database.collection).toBeCalledWith("test");
      expect(insert).toBeCalled();
      expect(insert).toBeCalledWith({ abc: "def" });
    });
    it("should call find", () => {
      const x = mc.find("test", {});
      expect(mc.database.collection).toBeCalled();
      expect(mc.database.collection).toBeCalledWith("test");
      expect(find).toBeCalled();
      expect(find).toBeCalledWith({});
    });
  });
});
