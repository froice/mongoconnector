# Froice Mongo Connector

## Install

```bash
  npm install @froice/mongoconnector
```

## Usage

### Create a document

```javascript
const config = {
  mongoURL: ["localhost:27017"],
  mongoUsername: "user",
  mongoPassword: "Password123",
  mongoDatabase: "userAuthenticationDatabase",
  mongoAuth: true,
  mongoAuthSource: "userAuthenticationDatabase"
}
const mc = new MongoConnector(config);

mc.create('user', {
  username: "abc",
  password: "def",
  groups: []
}).then(done => {
  // Do stuff
});
```

### Update a document

```javascript
const config = {
  mongoURL: ["localhost:27017"],
  mongoUsername: "user",
  mongoPassword: "Password123",
  mongoDatabase: "userAuthenticationDatabase",
  mongoAuth: true,
  mongoAuthSource: "userAuthenticationDatabase"
}
const mc = new MongoConnector(config);

mc.update('user', {
  username: "abc"
}, {
  $addToSet: {
    groups: "def"
  }
}).then(done => {
  // Do stuff
});
```

### Find a document

```javascript
const config = {
  mongoURL: ["localhost:27017"],
  mongoUsername: "user",
  mongoPassword: "Password123",
  mongoDatabase: "userAuthenticationDatabase",
  mongoAuth: true,
  mongoAuthSource: "userAuthenticationDatabase"
}
const mc = new MongoConnector(config);

mc.find('user', {
  username: "abc"
}).then(users => {
  // Do stuff
});
```

### Delete a document

```javascript
const config = {
  mongoURL: ["localhost:27017"],
  mongoUsername: "user",
  mongoPassword: "Password123",
  mongoDatabase: "userAuthenticationDatabase",
  mongoAuth: true,
  mongoAuthSource: "userAuthenticationDatabase"
}
const mc = new MongoConnector(config);

mc.delete('user', {
  username: "abc"
}).then(done => {
  // Do stuff
});
```